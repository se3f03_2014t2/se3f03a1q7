package main

import (
	. "github.com/smartystreets/goconvey/convey"

	. "gitlab.com/se3f03_2014t2/se3f03base"

	"testing"
)

func TestMakeDirList(t *testing.T) {
	Convey("Should Return the right list for a flat structure", t, func() {
		So(MakeDirList("tests/test3"), ShouldEqual, `/KeEP
/LittLe
/keep
/little
`)
	})
	Convey("Should Return the right list of hiearchies", t, func() {
		So(MakeDirList("tests/test1"), ShouldEqual, `/BIG
/ComBoS
/ComBosAnd123
/little
`)
		So(MakeDirList("tests/"), ShouldEqual, `/test1
/test1/BIG
/test1/ComBoS
/test1/ComBosAnd123
/test1/little
/test1.verbose
/test1.verbose/BIG
/test1.verbose/ComBoS
/test1.verbose/ComBosAnd123
/test1.verbose/little
/test2
/test2/BIG
/test2/ComBoS
/test2/ComBosAnd123
/test2/little
/test3
/test3/KeEP
/test3/LittLe
/test3/keep
/test3/little
`)
	})
}

func TestMakeDetailedDirList(t *testing.T) {
	Convey("Should return tests/test3 unedited contents", t, func() {
		dirList := makeDetailedDirList("tests/test3")
		So(dirList, ShouldNotEqual, nil)
		So(len(dirList), ShouldEqual, 4)
		
		So(dirList[0].Name, ShouldEqual, "/KeEP")
		So(dirList[0].Contents, ShouldEqual, "4")
		
		So(dirList[1].Name, ShouldEqual, "/LittLe")
		So(dirList[1].Contents, ShouldEqual, "2")
		
		So(dirList[2].Name, ShouldEqual, "/keep")
		So(dirList[2].Contents, ShouldEqual, "3")
		
		So(dirList[3].Name, ShouldEqual, "/little")
		So(dirList[3].Contents, ShouldEqual, "1")
	})
}
