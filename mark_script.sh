#!/bin/bash

MYPWD=`pwd`
if [ -n "$1" ]; then
	cd $1
fi

for i in *;do FOLDER=$(find $i -name fnameCapSmall* -printf '%h\n'|head -n1); [ -z $FOLDER ] || { docker run -i --rm=true -v `pwd`/$FOLDER:/under_test:ro mjdsys/se3f03a1q7 qchecker /under_test 2>&1|tee $i/A1q7.out;echo $i,${PIPESTATUS[0]} >>$MYPWD/marks; }; echo -e \\n\\n\\n\\n\\n; done
