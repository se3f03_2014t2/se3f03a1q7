.PHONY:	all
all:	docker se3f03A1Q7

se3f03A1Q7:	*.go ../se3f03base/*.go
	go build

docker:	se3f03A1Q7
	docker build -t mjdsys/se3f03a1q7 .
