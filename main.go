package main 

import (
	. "gitlab.com/se3f03_2014t2/se3f03base"
	"fmt"
	"io/ioutil"
	"os"
)

type fileInfo struct {
	Name    string
	Contents string
}

// Note that this function ignores folders for returning.
func makeDetailedDirList(dirRoot string) []fileInfo {
	var inner (func(string) []fileInfo)
	inner = func(subdir string) (ret []fileInfo) {
		infos, err := ioutil.ReadDir(dirRoot + subdir)
		if err != nil {
			fmt.Println("Had error in opening dir", dirRoot, ":", err)
			return nil
		}
		for _, info := range infos {
			if info.IsDir() {
				fmt.Println(subdir + info.Name())
				ret = append(ret, inner(subdir + info.Name() + "/")...)
			} else {
				name := subdir + info.Name()
				contents, err := ioutil.ReadFile(dirRoot + name)
				if err != nil {
					fmt.Printf("Failed to get file contents (returning what I got), err: %s", err)
				}
				ret = append(ret, fileInfo{ Name: name, Contents: string(contents) })
			}
		}
		return ret
	}
	
	return inner("/")
}

func runQuickTest(points int, verifyOutput bool, scriptName, ans string, args ...string) {
	dir := args[len(args)-1]
	fmt.Println("Inside of: ", dir)
	output := ExecuteScript(append([]string{scriptName}, args...)...)
	fmt.Printf("Got script output:\n%s", output)

	dirList := MakeDirList(dir)
	if CompareNewlineStrings(dirList, ans) {
		fmt.Println("Success!")
		if verifyOutput {
			if len(output) < 15 {
				fmt.Println("Expected more output for verbose!")
			} else {
				GM.Lock()
				Grade += points
				GM.Unlock()
			}
		} else {
			GM.Lock()
			Grade += points
			GM.Unlock()
		}
	} else {
		fmt.Println("Wrong directory layout.  Wanted:\n", ans, "\nGot:\n", dirList)
	}
}

// This expects the result folder to be a very specific output.  It makes the final result easier to
// check.
func runInteractiveTest(points int, scriptName string, args ...string) {
	dir := args[len(args)-1]
	fmt.Printf("Inside of %s with interaction.  Script output will be outputted twice below to allow for the marker to read your output.\n", dir)
	output := ExecuteScriptWithStdin(append([]string{scriptName}, args...)...)
	fmt.Printf("Got script output:\n%s", output)
	
	var singleLittleAt2, oldKeepAt3, singleOtherAt4, noOthers bool
	noOthers = true

	dirList := makeDetailedDirList(dir)
	for _, info := range dirList {
		if info.Name == "/little" && info.Contents == "2" {
			singleLittleAt2 = true
		} else if info.Name == "/keep" && info.Contents == "3" {
			oldKeepAt3 = true
		} else if info.Contents == "4" { // Captures any possible rename of /keep
			if singleOtherAt4 {
				noOthers = false
			} else {
				singleOtherAt4 = true
			}
		} else {
			noOthers = false // Any other file fails.
		}
	}

	if singleLittleAt2 && oldKeepAt3 && singleOtherAt4 && noOthers {
		fmt.Println("Success!")
		GM.Lock()
		Grade += points
		GM.Unlock()
	} else {
		fmt.Println("Wrong directory layout.  Wanted:\n", "", "\nGot:\n", dirList)
		fmt.Println("Detection failed at:")
		if !singleLittleAt2 {
			fmt.Println(" - Only a single /little file should exist, with contents of 2")
		}
		if !oldKeepAt3 {
			fmt.Println(" - The old /keep file should still be there, contents of 3")
		}
		if !singleOtherAt4 {
			fmt.Println(" - The other /keep should exist as a different file (name NOT checked), contents of 4")
		}
		if !noOthers {
			fmt.Println(" - A different file the three expected above was found.")
		}
	}
}

func main() {
	folder := os.Args[1]
	folderfd, err := os.Open(folder)
	if err != nil {
		fmt.Println("Failed to Open dir: ", err)
		os.Exit(-1)
	}
	defer folderfd.Close()
	files, err := folderfd.Readdirnames(-1)
	if err != nil {
		fmt.Println("Failed to Readdirnames: ", err)
		os.Exit(-1)
	}
	
	scriptName := folder + "/" + FindScript("fnameCapSmall", files)
	if scriptName == "/under_test/" {
		fmt.Println("Failed to find fnameCapSmall, only found files (should never happen): ", files)
		os.Exit(-2)
	} else {
		fmt.Println("Running: ", scriptName)
	}
	
	runQuickTest(4, false, scriptName, `/big
/combos
/combosand123
/little
`, "/tests/test1/")
	
	runQuickTest(3, true, scriptName, `/big
/combos
/combosand123
/little
`, "-v", "/tests/test1.verbose/")
	
	runQuickTest(2, false, scriptName, `/BIG
/COMBOS
/COMBOSAND123
/LITTLE
`, "-c", "/tests/test2/")
	
	runInteractiveTest(1, scriptName, "-v", "/tests/test3/")
	
	/*runTest(2, scriptName, "/usr/bin/top\n/bin/sync\n", "top", "sync")*/
	
	os.Exit(Grade)
}
